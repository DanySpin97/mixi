# Copyright 2016-2018 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A set of minimalistic tools to create a s6-based init system, including a /sbin/init binary, on a Linux kernel."
HOMEPAGE="http://skarnet.org/software/${PN}/"
DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    static
"

DEPENDENCIES="
    build+run:
        dev-libs/skalibs[>=2.6.0.0]
    run:
        dev-lang/execline[>=2.3.0.4]
        sys-apps/s6-linux-utils[>=2.4.0.2]
        sys-apps/s6-portable-utils[>=2.2.1.1]
        sys-apps/s6[>=2.7.1.0]
"

BUGS_TO="mixi@exherbo.org"

# CROSS_COMPILE: because configure only expects executables to be prefixed if build != host
# REALCC: because the makefile prefixes configure's CC with $(CROSS_COMPILE)
DEFAULT_SRC_COMPILE_PARAMS=(
    REALCC=${CC}
    CROSS_COMPILE=$(exhost --target)-
)

src_prepare()
{
    default

    edo sed 's/0700/0755/g' -i package/modes
}

src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable static allstatic)
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

src_install()
{
    default

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*
}

