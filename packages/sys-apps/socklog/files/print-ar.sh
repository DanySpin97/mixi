#!/bin/sh

cat <<EOF
rm -f "\${1}"
${AR} cr "\${@}"
${RANLIB} "\${1}"
EOF
