# Copyright 2015-2018 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="A dependency-based init script management system"
DESCRIPTION="
s6-rc is a suite of programs designed to help Unix distributions manage
services provided by various software packages, and automate initialization,
shutdown, and more generally changes to the machine state.

It keeps track of the complete service dependency tree and automatically
brings services up or down to reach the desired state.

In conjunction with s6, it ensures that long-lived services are
supervised, and that short-lived instructions are run in a reproducible
manner.
"
HOMEPAGE="http://skarnet.org/software/${PN}/"
DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    static
"

DEPENDENCIES="
    build+run:
        dev-libs/skalibs[>=2.6.2.0]
        dev-lang/execline[>=2.3.0.4]
        sys-apps/s6[>=2.6.2.0]
"

BUGS_TO="mixi@exherbo.org"

# CROSS_COMPILE: because configure only expects executables to be prefixed if build != host
# REALCC: because the makefile prefixes configure's CC with $(CROSS_COMPILE)
DEFAULT_SRC_COMPILE_PARAMS=(
    REALCC=${CC}
    CROSS_COMPILE=$(exhost --target)-
)

src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable static allstatic)
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

src_install()
{
    default

    insinto /usr/share/doc/${PNVR}/html
    doins doc/*

    # dodoc doesn't preserve symlinks, the empty directories are there for a reason
    edo cp -Pr examples "${IMAGE}"/usr/share/doc/${PNVR}
    keepdir /usr/share/doc/${PNVR}/examples/source/sshd-4/data/rules/ip4/192.168.0.0_22
    keepdir /usr/share/doc/${PNVR}/examples/source/{sshd-4,smtpd-4,dns-cache}/data/rules/ip4/0.0.0.0_0
    keepdir /usr/share/doc/${PNVR}/examples/source/udhcpc-eth3/env
}

